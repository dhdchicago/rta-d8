<?php
/**
 * @file
 * Contains \Drupal\resume\Form\WorkForm.
 */
namespace Drupal\trip_planner\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;

class AdminForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
 protected function getEditableConfigNames(){
   return [
    'trip_planner.adminsettings',
   ];
 }

 public function getFormId() {
   return 'trip_form';
 }

   /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('trip_planner.adminsettings');

    $form['trip_planner_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trip Title'),
      '#description' => $this->t('Title of Trip Planner'),
      '#default_value' => $config->get('trip_planner_title'),
    ];
    $form['trip_planner_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trip URL'),
      '#description' => $this->t('Base url of tripl planner redirect'),
      '#default_value' => $config->get('trip_planner_api'),
    ];
    $form['trip_planner_schedules'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Schedule URL'),
      '#description' => $this->t('URL for Schedule button under trip planner form'),
      '#default_value' => $config->get('trip_planner_schedules'),
    ];
    $form['trip_planner_trackers'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tracker URL'),
      '#description' => $this->t('URL for Tracker button under trip planner form'),
      '#default_value' => $config->get('trip_planner_trackers'),
    ];
    $form['trip_planner_maps'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maps URL'),
      '#description' => $this->t('URL for Maps button under trip planner form'),
      '#default_value' => $config->get('trip_planner_maps'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('trip_planner.adminsettings')
      ->set('trip_planner_title', $form_state->getValue('trip_planner_title'))
      ->set('trip_planner_api', $form_state->getValue('trip_planner_api'))
      ->set('trip_planner_schedules', $form_state->getValue('trip_planner_schedules'))
      ->set('trip_planner_trackers', $form_state->getValue('trip_planner_trackers'))
      ->set('trip_planner_maps', $form_state->getValue('trip_planner_maps'))
      ->save();
  }
}
