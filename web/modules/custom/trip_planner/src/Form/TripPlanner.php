<?php
/**
 * @file
 * Contains \Drupal\resume\Form\WorkForm.
 */
namespace Drupal\trip_planner\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Routing\TrustedRedirectResponse;

class TripPlanner extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tripplanner';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('trip_planner.adminsettings'); //admin form configs
    /*<input type="hidden" value="false" id="advanced" name="advanced"/>
            <input type="hidden" value="true" id="newTrip" name="newTrip"/>
            <input type="hidden" value="true" id="newRequest" name="newRequest"/>
            <input type="hidden" id="origData" name="origData" />
            <input type="hidden" id="destData" name="destData" />
            <input type="hidden" value="1" id="trip" name="trip"/>
            <input type="hidden" name="mode" id="mode">
            <input id="train" name="train" type="hidden" value="true" />
            <input id="bus" name="bus" type="hidden" value="true" />
            <input id="drive" type="hidden" name="drive" value="false" />
            <input id="bicycleOptionsRequired" name="bicycleOptionsRequired" type="hidden" value="false" />
            <input id="walkingOptionsRequired" name="walkingOptionsRequired" type="hidden" value="true" />
            <input id="driveToTransit" name="driveToTransit" type="hidden" value="false" />
            <input id="arriveLeave" name="arriveLeave" type="hidden" value="dep" />
            <input type="hidden" id="hour" name="hour" />
            <input type="hidden" id="minute" name="minute" />
            <input type="hidden" id="ampm" name="ampm" />
            <input type="hidden" id="travelDate" name="travelDate" />*/

    $title = $config->get('trip_planner_title');
    if($title){
      $form['title'] = array(
        '#type' => 'markup',
        '#markup' => '<p>Get Directions with: <h2>'.$title.'</h2></p>',
        '#prefix' => '<div id="trip_planner_form">',
      );
    }

    $form['originalAddy'] = array(
      '#type' => 'hidden',
      '#attributes' => array(
        'placeholder' => t('From'),
        'id' => array(
          'origAddress'
        ),
        'class' => array(
          //'hidden-lg',
          //'hidden-md',
          //'hidden-sm',
          //'hidden-xs'
        ),
      ),
      '#maxlength' => 255,
      //'#required' => True,
    git );
    $form['originalAdd'] = array(
      '#type' => 'textfield',
      //'#title' => 'From',
      '#attributes' => array(
        'placeholder' => t('From'),
        'class' => array(
          'suggest'
        ),
        'id' => array(
          'name_origin'
        ),
      ),
      '#required' => True,
    );

    $form['destAddy'] = array(
      '#type' => 'hidden',
      '#attributes' => array(
        'placeholder' => t('To'),
        'id' => array(
          'destAddress'
        ),
        'class' => array(
          //'hidden-lg',
          //'hidden-md',
          //'hidden-sm',
          //'hidden-xs'
        ),
      ),
      //'#required' => True,
    );

    $form['destAdd'] = array(
      '#type' => 'textfield',
      //'#title' => 'To',
      '#attributes' => array(
        'placeholder' => t('To'),
        'class' => array(
          'suggest'
        ),
        'id' => array(
          'name_destination'
        ),
      ),
      '#required' => True,
    );

    $form['service'] = array(
      '#type' => 'radios',
      '#title' => t('Leaving or Arriving'),
      '#default_value' => 'dep',
      '#options' => array(
        'dep' => $this->t('Leaving'),
        'arr' => $this->t('Arriving'),
      ),
    );
    $form['date_time'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="dates-trip">',
      //'#collapsible' => FALSE,
      //'#collapsed' => FALSE,
    );
    date_default_timezone_set('America/Chicago');
    $time = date("g:ia");
    $date = date("m/d/Y");

    $form['date_time_d'] = array(
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => $date,
      '#attributes' => array(
        'placeholder' => $date,
        'class' => array(
          'date-field'
        ),
      ),
      //'#field_prefix' => '',
      //'#field_suffix' => '€',
      //'#input_group' => TRUE,
    );

    $form['date_time_t'] = array(
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => $time,
      '#attributes' => array(
        'placeholder' => $time,
        'class' => array(
          'time-field',
          'bfh-timepicker'
        ),
      ),
      //'#field_prefix' => '',
      //'#field_suffix' => '€',
      //'#input_group' => TRUE,
    );
    $form['date_time_close'] = array(
      '#type' => 'markup',
      '#suffix' => '</div>',
      //'#collapsible' => FALSE,
      //'#collapsed' => FALSE,
    );
/*
    $form['accessible'] = array (
      '#type' => 'radios',
      '#title' => t('Accessible Trip'),
      '#default_value' => 0,
      '#options' => array(
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      )
    );
*/
/*
//full form options
    $form['checkboxes'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Transportation Mode Preferences:'),
    );
    $form['checkboxes']['train'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Train'),
      '#default_value' => TRUE,
    );

    $form['checkboxes']['bus'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Bus'),
      '#default_value' => TRUE,
    );
    $form['checkboxes']['driving'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Driving'),
      '#default_value' => FALSE,
    );
    $form['checkboxes']['bicycling'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Bicycling'),
      '#default_value' => FALSE,
    );
    $form['checkboxes']['walking'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Walking'),
      '#default_value' => TRUE,
    );
    $form['checkboxes']['drive_to_transit'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Drive to Transit'),
      '#default_value' => FALSE,
    );
    */
//end of form options
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Take Me There'),
        '#button_type' => 'primary',
        '#prefix' => '<br> 
         <div class="tooltip-parent" 
         aria-label="For more options in the next screen including Wheelchair Accessibility, click on Schedule Explorer then Route Options in Google Maps">
         <span class="tooltiptext"> For more options in the next screen including Wheelchair Accessibility, click on Schedule Explorer then Route Options in Google Maps </span>',
        '#suffix' => '</div>
                      <br>
                    </div>',
        //'#submit' => array('submit_redirect_submit'),
      );

      //$schedules = $config->get('trip_planner_schedules');
      $trackers = $config->get('trip_planner_trackers');
      $trips = $config->get('trip_planner_maps');

      /*if($schedules){
        $form['schedules'] = array(
          '#type' => 'link',
          '#title' => 'Schedule',
          '#url' => Url::fromUri($config->get('trip_planner_schedules')),
        );
      } */
      if($trackers){
        $form['trackers'] = array(
          '#type' => 'link',
          '#title' => 'Trackers & Schedules',
          '#prefix' => '<div id="trip_planner_buttons">',
          '#url' => Url::fromUri($config->get('trip_planner_trackers')),
        );
      }

      if($trips){
        $form['maps'] = array(
          '#type' => 'link',
          '#title' => 'Maps',
          '#suffix' => '</div>',
          '#url' => Url::fromUri($config->get('trip_planner_maps')),
        );
      }

      //drupal_set_message($config->get('trip_maps'));
    $form['#attached']['library'][] = 'trip_planner/tripplanner';

    //kint($form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {

      //if (strlen($form_state->getValue('candidate_number')) < 10) {
        //$form_state->setErrorByName('candidate_number', $this->t('Mobile number is too short.'));
      //}

    }

  /**
   * {@inheritdoc}
   */
 public function submitForm(array &$form, FormStateInterface $form_state) {

   //$form_state->setRedirect('http://elb-jpinstances-1463028547.us-east-1.elb.amazonaws.com/ccg3/XSLT_TRIP_REQUEST2');
    //foreach ($form_state->getValues() as $key => $value) {
      //$urlTrip = $config->get('trip_planner_api');
      $value = $form_state->getValue(['destAdd']);
      $time = $form_state->getValue(['date_time_t']);
      $date = $form_state->getValue(['date_time_d']);
      $startLocation = $form_state->getValue(['originalAdd']);
      $endLocation = $form_state->getValue(['destAdd']);
      $orgAddID = $form_state->getValue(['originalAddy']);
      $destAddID = $form_state->getValue(['destAddy']);
      $depart = $form_state->getValue(['service']);
      
      /*
      //xtra options
      $train = $form_state->getValue(['train']);
      $bus  = $form_state->getValue(['bus']);
      $driving = $form_state->getValue(['driving']);
      $bicycle = $form_state->getValue(['bicycling']);
      $walking = $form_state->getValue(['walking']);
      $transit = $form_state->getValue(['drive_to_transit']);
      $accessible = $form_state->getValue(['accessible']);
      //will have to do better later
      //drupal form api would not allow set default value true or false

      if($train == 1){
        $train = 'true';
      } else{
        $train = 'false';
      }

      if($bus == 1){
        $bus = 'true';
      } else{
        $bus = 'false';
      }

       if($driving == 1){
        $driving = 'true';
      }else{
        $driving = 'false';
      }

       if($bicycle == 1){
        $bicycle = 'true';
      }else{
        $bicycle = "false";
      }

      if($walking == 1){
        $walking = 'true';
      } else{
        $walking = 'false';
      }

      if($transit == 1){
        $transit = 'true';
      } else{
        $transit = 'false';
      }
      */

      //$dates = explode('/', $date);
      $hours = explode(':', $time);

      //get mins
      preg_match('/\d+/', $hours[1], $numMatch);
      $mins = $numMatch[0];

      //get am pm from
      preg_match('/[^\d]+/', $hours[1], $textMatch);
      $pm = $textMatch[0];

      //$ldate = $dates[1].$dates[0].$dates[2];
      $ldate = $date;    /*Inputting date without splitting forward slash */
      $start = explode(',', $startLocation);
      $end = explode(',', $endLocation);
      $orgAdd = preg_replace('/[^a-zA-Z0-9]/', ' ', $startLocation);
      $destAdd = preg_replace('/[^a-zA-Z0-9]/', ' ', $endLocation);

      $orgAdd = preg_replace('#\s+#', '+', $orgAdd);
      $destAdd = preg_replace('#\s+#', '+', $destAdd);


      if(empty($orgAddID)){
        $orgAddID = $orgAdd;
      }
      if(empty($destAddID)){
        $destAddID = $destAdd;
      }

      $hoursint = intval($hours[0]);
      if($pm == 'pm'){  
        if ($hoursint == 12){$int = $hoursint;}
        else {$int = $hoursint + 12;}    // Only for 1pm and onwards
      } 
      else{
        if ($hoursint == 12){$int = 00;}
        else {$int = $hours[0];}
      }

        
      //drupal_set_message($orgAddID.' '.$destAddID);
     /* $response = new TrustedRedirectResponse('https://tripplanner.rtachicago.org/ccg3/XSLT_TRIP_REQUEST2?actn=entry
      &advanced=false
      &newTrip=true
      &newRequest=true
      &origData=&destData=&trip=1
      &mode=&train='.$train.'&bus='.$bus.'&drive='.$driving.'&bicycleOptionsRequired='.$bicycle.
      '&walkingOptionsRequired='.$walking.'&driveToTransit='.$transit.'&arriveLeave=dep&hour='.$hours[0].'&minute='.$mins.'&ampm='.$pm.'&travelDate=&language=en&itdLPxx_directRequest=1&sessionID=0&std3_commonMacro=trip&type_origin=any&type_destination=any&itdLPxx_timeFormat=12&nameDefaultText_origin=From&nameDefaultText_destination=To&origAddress=From&name_origin='.$orgAdd.'&nameInfo_origin='.$orgAddID.'&destAddress=To&name_destination='.$destAdd.'&nameInfo_destination='.$destAddID.'&itdTripDateTimeDepArr='.$depart.'&itdDateDayMonthYear='.$ldate.'&itdTime='.$int.'%3A'.$mins.'&std3_accesibilityNeededMacro='.$accessible);*/
   
   /*Show all transit options in case selection is current date/time AND for Departure ONLY*/
   
   $currentTime = explode(':', date("g:ia"));
   //get current mins
   preg_match('/\d+/', $currentTime[1], $currNumMins);
   $currentMins = $currNumMins[0];

   //get am pm from
   preg_match('/[^\d]+/', $currentTime[1], $textMatch);
   $amPm = $textMatch[0];

   $tempHours = intval($currentTime[0]);
   if($amPm == 'pm'){
      if ($tempHours == 12){ $currentHour = $tempHours;}
      else {$currentHour = $tempHours + 12;}
    } 
    else{
       if ($tempHours == 12){$currentHour = 00;}
       else {$currentHour = $currentTime[0];}
    }

   //If offset of chosen vs current time is witin 10 mins than show current routes
   $minDiff  = abs ($mins - $currentMins);
   $hourDiff = abs ($int - $currentHour);

   if ($ldate == date("m/d/Y") && $depart == 'dep' && 
      (($hourDiff == 0 && $minDiff < 11) || 
      ($hourDiff < 2 && $minDiff > 49 && ( 
        ($mins > $currentMins && $int < $currentHour) || 
        ($mins < $currentMins && $int > $currentHour) ) ))) {
      $depart = 'now';
   }
    

   /*Google Maps String Query*/
    $response = new TrustedRedirectResponse
    ('https://google.com/maps?ie=UTF8&f=d&sll=41.880255,-88.11629&sspn=1.30,1.450195&saddr='.$orgAdd.'&daddr='.$destAdd.'&ttype='.$depart.'&date='.$ldate.'&time='.$int.':'.$mins.'&dirflg=r');

    $form_state->setResponse($response);
    
  }
}