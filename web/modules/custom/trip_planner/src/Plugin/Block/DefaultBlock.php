<?php

namespace Drupal\trip_planner\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DefaultBlock' block.
 *
 * @Block(
 *  id = "default_block",
 *  admin_label = @Translation("Default block"),
 * )
 */
class DefaultBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    //$build = [];
   // $build['default_block']['#markup'] = 'Implement DefaultBlock.';
    $build = [];
    $build['heading'] = [
    ];

    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\trip_planner\Form\TripPlanner');
    //return $form;

    return $build;
  }

}
