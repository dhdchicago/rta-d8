jQuery(function($){

    //bounds of area
        defaultBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(41.271614, -89.044619),
        new google.maps.LatLng(42.504503, -87.26758)
        );

        //addinbg geocomplete
        $("#edit-originaladdy").geocomplete({
          bounds: defaultBounds,
          types: ['geocode', 'establishment'],
        });

        $("#edit-destaddy").geocomplete({ 
          bounds: defaultBounds,
          //type: ['restaurant']
        });

        //date and time picker
        $('.time-field').timepicker({ 'scrollDefault': 'now' });
        $('.date-field').datepicker({ 
          format: 'mm/dd/yyyy',
          startDate: '-3d'
      });

        //suggest
        /*$('.suggest').mdvOdvSuggest({
          stopfinder_url: '//tripplanner.rtachicago.org/ccg3/XSLT_STOPFINDER_REQUEST?',
          template: '<li class="std3_sug_-cls-">-name-</li>',
          mode: 'suggesthistory',
          http_params : {
            std3_suggestMacro: 'std3_suggest',
                  outputFormat: 'json'
          },
              item_name_renderer : function (item) {
                  var name = item.object || item.name,
                      place = (item.ref && item.ref.place) || '';

                  if (item.buildingNumber) {
                      name = item.buildingNumber + ' ' + name;
                  }

                  name = name.replace('$XINT$', '&');

                  if (place) {
                      return '<strong>' + name + '</strong><br/>' + place;
                  }

                  return name;
              },
              item_input_val_renderer: function (item) {
                  var name = item.object || item.name,
                      place = (item.ref && item.ref.place) || '';

                  if (item.buildingNumber) {
                      name = item.buildingNumber + ' ' + name;
                  }

                  name = name.replace('$XINT$', '&');

                  if (place) {
                      name = name + ', ' + place;
                  }

                  return name;
              }
        });
        */

        //pulling location id from dom created element
          $('.form-item-originaladd').bind("DOMSubtreeModified",function(){
            var $ogid = $('#nameInfo_originalAdd').attr('value');
            console.log($ogid);
            $('#origAddress').attr('value',$ogid);
          });
          $('#name_origin').blur(function(){
            if($('#nameInfo_originalAdd').length > 0){
              var $ogid = $('#nameInfo_originalAdd').attr('value');
              console.log($ogid);
              $('#origAddress').attr('value',$ogid);
            };
          });

        //pull location id for destination
        $('.form-item-destadd').bind("DOMSubtreeModified",function(){
          var $ogid = $('#nameInfo_destAdd').attr('value');
          console.log($ogid);
          $('#destAddress').attr('value',$ogid);
        });
        $('#name_destination').blur(function(){
          if($('#nameInfo_destAdd').length > 0){
            var $ogid = $('#nameInfo_destAdd').attr('value');
            console.log($ogid);
            $('#destAddress').attr('value',$ogid);
          };
        });

        function reverseTrip() {
          var e = document.getElementById('#edit-candidate-name').value;
          var i = document.getElementById('destAddress').value;
          document.getElementById('#edit-candidate-name').value = i;
          document.getElementById('destAddress').value = e;
        }
        function submit(){
          //$('#tripplanner').submit();
        }
        function parseTime(){
          // Clean up the time
          var timeVal = document.getElementById('timepicker').value;
          var hourVal = timeVal.substr(0, timeVal.indexOf(':'));
          var minuteVal = timeVal.substr(timeVal.indexOf(':')+1);
          var ampmVal = 'am';
          if (hourVal >= 12) {
            hourVal = hourVal - 12;
            var ampmVal = 'pm';
          }
          document.tripPlan.hour.value=hourVal;
          document.tripPlan.minute.value=minuteVal;
          document.tripPlan.ampm.value=ampmVal;
          // Clean up the origin and destination addresses
          var destAddress = document.getElementById('edit-destaddy').value;
          var origAddress = document.getElementById('edit-originaladdy').value;
          console.log(destAddress);
          // Strip out country
          if (origAddress.indexOf(', United States') > -1) {
              origAddress = origAddress.substr(0, origAddress.indexOf(', United States'));
          }
          if (destAddress.indexOf(', United States') > -1) {
              destAddress = destAddress.substr(0, destAddress.indexOf(', United States'));
          }
          // Strip out state
          if (origAddress.indexOf(', IL') > -1) {
              origAddress = origAddress.substr(0, origAddress.indexOf(', IL'));
          }
          if (destAddress.indexOf(', IL') > -1) {
              destAddress = destAddress.substr(0, destAddress.indexOf(', IL'));
          }
          document.tripPlan.origAddress.value = origAddress;
          document.tripPlan.destAddress.value = destAddress;
          // Clean up the travel date
          var date = document.getElementById('travelday').value;
          if (Modernizr.inputtypes['date']) {
            var year = date.substring(0, 4);
            var month = date.substring(5, 7);
            var day = date.substring(8, 10);
            date = month + '/' + day + '/' + year;
          }
          else if (date.indexOf('-') != -1) {
            var year = date.substring(0, 4);
            var month = date.substring(5, 7);
            var day = date.substring(8, 10);
            date = month + '/' + day + '/' + year;
          }
          document.tripPlan.travelDate.value = date;
          // Submit the form
          document.tripPlan.submit();
        }
        jQuery(function($) {
          document.addEventListener('DOMNodeInserted', function(event) {
            var target = $(event.target);
            if (target.hasClass('pac-item')) {
              target.html(target.html().replace(/, United States<\/span>$/, "</span>"));
            }
          });
          $("travelday").on('ready', function() {
            if (!Modernizr.inputtypes['date']) {
            var year = date.substring(0, 4);
            var month = date.substring(5, 7);
            var day = date.substring(8, 10);
            date = month + '/' + day + '/' + year;
          }
          });
        });
});
