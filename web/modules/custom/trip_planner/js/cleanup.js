function reverseTrip() {
  var e = document.getElementById('#edit-candidate-name').value;
  var i = document.getElementById('destAddress').value;
  document.getElementById('#edit-candidate-name').value = i;
  document.getElementById('destAddress').value = e;
}
function submit(){
  $('#tripplanner').submit();
}
function parseTime(){
  // Clean up the time
  var timeVal = document.getElementById('timepicker').value;
  var hourVal = timeVal.substr(0, timeVal.indexOf(':'));
  var minuteVal = timeVal.substr(timeVal.indexOf(':')+1);
  var ampmVal = 'am';
  if (hourVal >= 12) {
    hourVal = hourVal - 12;
    var ampmVal = 'pm';
  }
  document.tripPlan.hour.value=hourVal;
  document.tripPlan.minute.value=minuteVal;
  document.tripPlan.ampm.value=ampmVal;
  // Clean up the origin and destination addresses
  var origAddress = document.getElementById('#edit-candidate-name').value;
  var destAddress = document.getElementById('destAddress').value;
  // Strip out country
  if (origAddress.indexOf(', United States') > -1) {
      origAddress = origAddress.substr(0, origAddress.indexOf(', United States'));
  }
  if (destAddress.indexOf(', United States') > -1) {
      destAddress = destAddress.substr(0, destAddress.indexOf(', United States'));
  }
  // Strip out state
  if (origAddress.indexOf(', IL') > -1) {
      origAddress = origAddress.substr(0, origAddress.indexOf(', IL'));
  }
  if (destAddress.indexOf(', IL') > -1) {
      destAddress = destAddress.substr(0, destAddress.indexOf(', IL'));
  }
  document.tripPlan.origAddress.value = origAddress;
  document.tripPlan.destAddress.value = destAddress;
  // Clean up the travel date
  var date = document.getElementById('travelday').value;
  if (Modernizr.inputtypes['date']) {
    var year = date.substring(0, 4);
    var month = date.substring(5, 7);
    var day = date.substring(8, 10);
    date = month + '/' + day + '/' + year;
  }
  else if (date.indexOf('-') != -1) {
    var year = date.substring(0, 4);
    var month = date.substring(5, 7);
    var day = date.substring(8, 10);
    date = month + '/' + day + '/' + year;
  }
  document.tripPlan.travelDate.value = date;
  // Submit the form
  document.tripPlan.submit();
}
jQuery(function($) {
  document.addEventListener('DOMNodeInserted', function(event) {
    var target = $(event.target);
    if (target.hasClass('pac-item')) {
      target.html(target.html().replace(/, United States<\/span>$/, "</span>"));
    }
  });
  $("travelday").on('ready', function() {
    if (!Modernizr.inputtypes['date']) {
    var year = date.substring(0, 4);
    var month = date.substring(5, 7);
    var day = date.substring(8, 10);
    date = month + '/' + day + '/' + year;
  }
  });
});