<?php

namespace Drupal\clarity_editor\Plugins\CKEditorPlugin;

use Drupal\ckeditor\Plugin\CKEditorPlugin\Internal;
use Drupal\editor\Entity\Editor;

/**
 * Allow custom config settings.
 *
 * @CKEditorPlugin(
 *   id = "custom_internal",
 *   label = @Translation("Custom CKEditor core")
 * )
 */
class CustomInternal extends Internal {

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {

    $config = parent::getConfig($editor);

    // Put your custom configs here.
    $config['allowedContent'] = TRUE;

    return $config;
  }

}