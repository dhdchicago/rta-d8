var gulp = require('gulp');
var sass = require('gulp-sass'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-clean-css'),
    shell = require('gulp-shell'),
    notifier = require('node-notifier'),
    exec = require('child_process').exec;
var $    = require('gulp-load-plugins')();


gulp.task('sass', function() {
  return gulp.src('scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('css'))
    .pipe(notify({
      title: "SASS Compiled",
      message: "All SASS files have been recompiled to CSS.",
      onLast: true
    }));
});
gulp.task('compress', function() {
  return gulp.src('js/*.js')
    .pipe(uglify({
	    compress:true,
	    output: {beautify:true}
     }))
    //.pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('js-min'))
    .pipe(notify({
      title: "JS Minified",
      message: "All JS files in the theme have been minified.",
      onLast: true
    }));
});

gulp.task('drush', function(){
   return gulp.src('', {read: false})
    .pipe(shell([
      'lando drupal cr',
    ]))
    .pipe(notify({
      title: "Caches cleared",
      message: "Drupal CSS/JS caches cleared.",
      onLast: true
    }));
});

gulp.task('drush', function (cb) {
  exec('drush cr', function(err, stdout, stderr){
    console.log(stdout);
    console.log(stderr);
  cb(err);
  });
  notifier.notify({ title: 'Drush', message: 'Cache Clear Done' });
});

gulp.task('minify-css',() => {
  return gulp.src('css/*.css')
    .pipe(minify())
    .pipe(gulp.dest('css'))
    .pipe(notify({
      title: "CSS Minified",
      message: "CSS file has been minified.",
      onLast: true
    }));
});

gulp.task('watch', function() {
  gulp.watch(['scss/**/*.scss'], gulp.series('sass'));
  gulp.watch('js/**/*.js', gulp.series('compress', 'drush'));
  gulp.watch(['scss/**/*.scss'], gulp.series('drush'));
 // gulp.watch(['templates/**/*.twig'], ['drush']);
  //gulp.watch('css/**/*.css', ['minify-css']);
});

//gulp.task('default', ['watch']);
gulp.task('default', gulp.series('watch'));
