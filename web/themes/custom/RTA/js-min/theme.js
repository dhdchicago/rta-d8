!function(t, n, e) {
    n.icon || (n.icon = {
        bundles: {}
    }), n.theme.icon && !n.theme.prototype.icon || t.extend(n.theme, {
        icon: function(t, e, o) {
            return n.icon.bundles[t] ? (o = Attributes(o).addClass("icon").set("aria-hidden", "true"), 
            e = n.icon.bundles[t](e, o), "<span" + o + "></span>") : "";
        }
    }), n.icon.bundles.bootstrap = function(t, n) {
        n.addClass([ "glyphicon", "glyphicon-" + t ]);
    }, t.extend(n.theme, {
        ajaxThrobber: function() {
            return n.theme.bootstrapIcon("refresh", {
                class: [ "ajax-throbber", "glyphicon-spin" ]
            });
        },
        button: function(t) {
            t = Attributes(t).addClass("btn");
            var n = t.get("context", "default"), o = t.get("value", "");
            return t.remove("context").remove("value"), t.hasClass([ "btn-default", "btn-primary", "btn-success", "btn-info", "btn-warning", "btn-danger", "btn-link" ]) || t.addClass("btn-" + e.checkPlain(n)), 
            "<button" + t + ">" + o + "</button>";
        },
        btn: function(t) {
            return n.theme("button", t);
        },
        "btn-block": function(t) {
            return n.theme("button", Attributes(t).addClass("btn-block"));
        },
        "btn-lg": function(t) {
            return n.theme("button", Attributes(t).addClass("btn-lg"));
        },
        "btn-sm": function(t) {
            return n.theme("button", Attributes(t).addClass("btn-sm"));
        },
        "btn-xs": function(t) {
            return n.theme("button", Attributes(t).addClass("btn-xs"));
        },
        bootstrapIcon: function(t, e) {
            return n.theme("icon", "bootstrap", t, e);
        }
    });
}(window.jQuery, window.Drupal, window.Drupal.bootstrap);