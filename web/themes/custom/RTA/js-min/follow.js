!function(e) {
    if (document.getElementById("subscribe-submit")) {
        if ("placeholder" in e.createElement("input")) {
            var t = e.querySelector("label[for=subscribe-field-blog_subscription-7]");
            t.style.clip = "rect(1px, 1px, 1px, 1px)", t.style.position = "absolute", t.style.height = "1px", 
            t.style.width = "1px", t.style.overflow = "hidden";
        }
        var i = e.getElementById("subscribe-blog-blog_subscription-7"), n = e.getElementById("subscribe-field-blog_subscription-7"), s = function(e) {
            if ("" === n.value) return n.focus(), e.preventDefault && e.preventDefault(), !1;
        };
        i && (window.addEventListener ? i.addEventListener("submit", s, !1) : i.attachEvent("onsubmit", s));
    }
}(document);