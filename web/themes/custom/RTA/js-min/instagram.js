!function(e, t) {
    function n(e) {
        var t, n = document.getElementsByClassName("mySlides");
        for (e > n.length && (e = 1), e < 1 && (e = n.length), t = 0; t < n.length; t++) n[t].style.display = "none";
        n.length > 0 && (n[e - 1].style.display = "block");
    }
    e.ajax({
        url: "https://api.instagram.com/v1/users/self/media/recent/?access_token=1758623324.f4ad936.b4f021fede03434fa77dbc7727fb7ac6",
        type: "GET",
        data: {},
        dataType: "json",
        success: function(e) {
            e.data.forEach(function(e) {
                var t = e.images.standard_resolution.url, n = e.caption.text, a = document.createElement("div"), d = document.createElement("img"), i = document.createElement("div"), c = document.createElement("a");
                n.length > 120 && (n = n.substring(0, 120) + "..."), d.src = t, c.setAttribute("href", "https://www.instagram.com/rta_chicago/"),
                c.setAttribute("target", "_blank"), c.appendChild(d), a.innerHTML = n, a.classList.add("text"),
                i.appendChild(c), i.appendChild(a), i.classList.add("mySlides", "fade");
                var l = document.getElementById("instaSlide");
                l && l.appendChild(i);
            });
            var t = document.createElement("div"), a = document.createElement("div");
            t.setAttribute("id", "prev"), a.setAttribute("id", "next"), a.innerHTML = "❯", t.innerHTML = "❮";
            var d = document.getElementById("instaSlide");
            if (d) {
                var i = d.childElementCount;
                d.appendChild(a), d.appendChild(t);
            }
            var c = 1;
            n(c), t.addEventListener("click", function() {
                c > 1 && n(c += -1);
            }, !1), a.addEventListener("click", function() {
                c <= i && n(c += 1);
            }, !1);
        }
    });
}(jQuery, Drupal);