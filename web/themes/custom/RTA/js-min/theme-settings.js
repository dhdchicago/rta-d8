!function(e, t) {
    "use strict";
    t.behaviors.bootstrapSettingSummaries = {
        attach: function(a) {
            var n = e(a);
            n.find("#edit-general").drupalSetSummary(function() {
                var e = [], a = n.find('select[name="button_size"] :selected');
                a.val() && e.push(t.t("@size Buttons", {
                    "@size": a.text()
                }));
                var i = n.find('select[name="image_shape"] :selected');
                return i.val() && e.push(t.t("@shape Images", {
                    "@shape": i.text()
                })), n.find(':input[name="image_responsive"]').is(":checked") && e.push(t.t("Responsive Images")), 
                n.find(':input[name="table_responsive"]').is(":checked") && e.push(t.t("Responsive Tables")), 
                e.join(", ");
            }), n.find("#edit-components").drupalSetSummary(function() {
                var e = [];
                parseInt(n.find('select[name="breadcrumb"]').val(), 10) && e.push(t.t("Breadcrumbs"));
                var a = "Navbar: " + n.find('select[name="navbar_position"] :selected').text();
                return n.find('input[name="navbar_inverse"]').is(":checked") && (a += " (" + t.t("Inverse") + ")"), 
                e.push(a), e.join(", ");
            }), n.find("#edit-javascript").drupalSetSummary(function() {
                var e = [];
                return n.find('input[name="modal_enabled"]').is(":checked") && e.push(t.t("Modals")), 
                n.find('input[name="popover_enabled"]').is(":checked") && e.push(t.t("Popovers")), 
                n.find('input[name="tooltip_enabled"]').is(":checked") && e.push(t.t("Tooltips")), 
                e.join(", ");
            }), n.find("#edit-advanced").drupalSetSummary(function() {
                var e = [], a = n.find('select[name="cdn_provider"] :selected'), i = a.val();
                if (a.length && i.length && (e.push(t.t("CDN provider: %provider", {
                    "%provider": a.text()
                })), "jsdelivr" === i)) {
                    var s = n.find('select[name="cdn_jsdelivr_version"] :selected');
                    s.length && s.val().length && e.push(s.text());
                    var r = n.find('select[name="cdn_jsdelivr_theme"] :selected');
                    r.length && "bootstrap" !== r.val() && e.push(r.text());
                }
                return e.join(", ");
            });
        }
    }, t.behaviors.bootstrapBootswatchPreview = {
        attach: function(a) {
            var n = e(a), i = n.find("#bootstrap-theme-preview");
            i.once("bootstrap-theme-preview").each(function() {
                i.append('<a id="bootstrap-theme-preview-bootstrap_theme" class="bootswatch-preview element-invisible" href="https://getbootstrap.com/docs/3.3/examples/theme/" target="_blank"><img class="img-responsive" src="//getbootstrap.com/docs/3.3/examples/screenshots/theme.jpg" alt="' + t.t("Preview of the Bootstrap theme") + '" /></a>'), 
                e.ajax({
                    url: "https://bootswatch.com/api/3.json",
                    dataType: "json",
                    success: function(e) {
                        for (var a = e.themes, n = 0, s = a.length; n < s; n++) i.append('<a id="bootstrap-theme-preview-' + a[n].name.toLowerCase() + '" class="bootswatch-preview element-invisible" href="' + a[n].preview + '" target="_blank"><img class="img-responsive" src="' + a[n].thumbnail.replace(/^http:/, "https:") + '" alt="' + t.t("Preview of the @title Bootswatch theme", {
                            "@title": a[n].name
                        }) + '" /></a>');
                    },
                    complete: function() {
                        i.parent().find('select[name="cdn_jsdelivr_theme"]').bind("change", function() {
                            i.find(".bootswatch-preview").addClass("visually-hidden"), e(this).val().length && i.find("#bootstrap-theme-preview-" + e(this).val()).removeClass("visually-hidden");
                        }).change();
                    }
                });
            });
        }
    }, t.behaviors.bootstrapContainerPreview = {
        attach: function(t) {
            var a = e(t), n = a.find("#edit-container");
            n.once("container-preview").each(function() {
                n.find('[name="fluid_container"]').on("change.bootstrap", function() {
                    e(this).is(":checked") ? a.find(".container").removeClass("container").addClass("container-fluid") : a.find(".container-fluid").removeClass("container-fluid").addClass("container");
                });
            });
        }
    }, t.behaviors.bootstrapNavbarPreview = {
        attach: function(t) {
            var a = e(t), n = a.find("#edit-navbar");
            n.once("navbar").each(function() {
                var t = a.find("body"), i = a.find("#navbar.navbar");
                n.find('select[name="navbar_position"]').bind("change", function() {
                    var a = e(this).find(":selected").val();
                    switch (i.removeClass("navbar-fixed-bottom navbar-fixed-top navbar-static-top container"), 
                    a.length ? i.addClass("navbar-" + a) : i.addClass("container"), t.removeClass("navbar-is-fixed-top navbar-is-fixed-bottom navbar-is-static-top"), 
                    a) {
                      case "fixed-top":
                        t.addClass("navbar-is-fixed-top");
                        break;

                      case "fixed-bottom":
                        t.addClass("navbar-is-fixed-bottom");
                        break;

                      case "static-top":
                        t.addClass("navbar-is-static-top");
                    }
                }), n.find('input[name="navbar_inverse"]').bind("change", function() {
                    i.toggleClass("navbar-inverse navbar-default");
                });
            });
        }
    };
}(jQuery, Drupal);