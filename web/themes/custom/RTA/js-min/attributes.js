!function(t, e) {
    var s = function(t) {
        this.data = t && e.isObject(t) && e.clone(t) || {};
    };
    s.prototype.toString = function() {
        var t, s, n = "", i = function(t) {
            return t && t.toString().replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;") || "";
        };
        for (t in this.data) this.data.hasOwnProperty(t) && (s = this.data[t], e.isFunction(s) && (s = s()), 
        e.isObject(s) && (s = e.values(s)), e.isArray(s) && (s = s.join(" ")), n += " " + i(t) + '="' + i(s) + '"');
        return n;
    }, s.prototype.addClass = function(t) {
        return t = [].concat(this.getClasses(), t), this.set("class", e.uniq(t)), this;
    }, s.prototype.exists = function(t) {
        return void 0 !== this.data[t] && null !== this.data[t];
    }, s.prototype.get = function(t, e) {
        return this.exists(t) || (this.data[t] = e), this.data[t];
    }, s.prototype.getData = function() {
        return e.clone(this.data);
    }, s.prototype.getClasses = function() {
        var t = [].concat(this.get("class", []));
        return e.uniq(t);
    }, s.prototype.hasClass = function(t) {
        t = [].concat(t);
        var s = this.getClasses(), n = !1;
        return e.each(t, function(t) {
            -1 !== e.indexOf(s, t) && (n = !0);
        }), n;
    }, s.prototype.merge = function(e, n) {
        return e = e instanceof s ? e.getData() : e, void 0 === n || n ? this.data = t.extend(!0, {}, this.data, e) : t.extend(this.data, e), 
        this;
    }, s.prototype.remove = function(t) {
        return this.exists(t) && delete this.data[t], this;
    }, s.prototype.removeClass = function(t) {
        return this.set("class", e.without(this.getClasses(), [].concat(t))), this;
    }, s.prototype.replaceClass = function(t, s) {
        var n = this.getClasses(), i = e.indexOf(t, n);
        return i >= 0 && (n[i] = s, this.set("class", n)), this;
    }, s.prototype.set = function(t, e) {
        return this.data[t] = e, this;
    }, window.Attributes = function(t) {
        return t instanceof s ? t : new s(t);
    };
}(window.jQuery, window._);