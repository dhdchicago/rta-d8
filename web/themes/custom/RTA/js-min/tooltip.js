var Drupal = Drupal || {};

!function(t, o, i) {
    "use strict";
    i.extendPlugin("tooltip", function(t) {
        return {
            DEFAULTS: {
                animation: !!t.tooltip_animation,
                html: !!t.tooltip_html,
                placement: t.tooltip_placement,
                selector: t.tooltip_selector,
                trigger: t.tooltip_trigger,
                delay: parseInt(t.tooltip_delay, 10),
                container: t.tooltip_container
            }
        };
    }), o.behaviors.bootstrapTooltips = {
        attach: function(o) {
            for (var i = t(o).find('[data-toggle="tooltip"]').toArray(), a = 0; a < i.length; a++) {
                var n = t(i[a]), l = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, n.data());
                n.tooltip(l);
            }
        },
        detach: function(o) {
            t(o).find('[data-toggle="tooltip"]').tooltip("destroy");
        }
    };
}(window.jQuery, window.Drupal, window.Drupal.bootstrap);