!function(t, n, e) {
    "use strict";
    n.bootstrap = {
        settings: e.bootstrap || {}
    }, n.bootstrap.checkPlain = function(t) {
        return t && n.checkPlain(t) || "";
    }, n.bootstrap.extendPlugin = function(n, e) {
        if (!t.fn[n] || !t.fn[n].Constructor) return !1;
        if (t.isFunction(e)) {
            var o = e.apply(t.fn[n].Constructor, [ this.settings ]);
            t.isPlainObject(o) && t.extend(!0, t.fn[n].Constructor, o);
        }
        return void 0 === t.fn[n].Constructor.prototype.option && (t.fn[n].Constructor.prototype.option = this.option), 
        t.fn[n].Constructor;
    }, n.bootstrap.replacePlugin = function(n, e) {
        if (!t.fn[n] || !t.fn[n].Constructor || !t.isFunction(e)) return !1;
        var o = t.fn[n].Constructor, r = e.apply(o);
        if (t.isFunction(r)) {
            r.Constructor = o;
            var i = t.fn[n];
            r.noConflict = function() {
                return t.fn[n] = i, this;
            }, t.fn[n] = r;
        }
    }, n.bootstrap.eventMap = {
        Event: /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
        MouseEvent: /^(?:click|dblclick|mouse(?:down|enter|leave|up|over|move|out))$/,
        KeyboardEvent: /^(?:key(?:down|press|up))$/,
        TouchEvent: /^(?:touch(?:start|end|move|cancel))$/
    }, n.bootstrap.simulate = function(e, o, r) {
        if ("function" == typeof t.simulate) return void new t.simulate(e, o, r);
        var i, s;
        for (var u in n.bootstrap.eventMap) if (n.bootstrap.eventMap[u].test(o)) {
            s = u;
            break;
        }
        if (!s) throw new SyntaxError("Only rudimentary HTMLEvents, KeyboardEvents and MouseEvents are supported: " + o);
        var a = {
            bubbles: !0,
            cancelable: !0
        };
        "KeyboardEvent" !== s && "MouseEvent" !== s || t.extend(a, {
            ctrlKey: !1,
            altKey: !1,
            shiftKey: !1,
            metaKey: !1
        }), "MouseEvent" === s && t.extend(a, {
            button: 0,
            pointerX: 0,
            pointerY: 0,
            view: window
        }), r && t.extend(a, r), "function" == typeof window[s] ? (i = new window[s](o, a), 
        e.dispatchEvent(i)) : document.createEvent ? (i = document.createEvent(s), i.initEvent(o, a.bubbles, a.cancelable), 
        e.dispatchEvent(i)) : "function" == typeof e.fireEvent ? (i = t.extend(document.createEventObject(), a), 
        e.fireEvent("on" + o, i)) : (e[o], e[o]());
    }, n.bootstrap.option = function(n, e) {
        var o, r, i, s = n;
        if (0 === arguments.length) return t.extend({}, this.options);
        if ("string" == typeof n) if (s = {}, o = n.split("."), n = o.shift(), o.length) {
            for (r = s[n] = t.extend({}, this.options[n]), i = 0; i < o.length - 1; i++) r[o[i]] = r[o[i]] || {}, 
            r = r[o[i]];
            if (n = o.pop(), 1 === arguments.length) return void 0 === r[n] ? null : r[n];
            r[n] = e;
        } else {
            if (1 === arguments.length) return void 0 === this.options[n] ? null : this.options[n];
            s[n] = e;
        }
        for (n in s) s.hasOwnProperty(n) && (this.options[n] = s[n]);
        return this;
    };
}(window.jQuery, window.Drupal, window.drupalSettings);