!function(e, t, o) {
    "use strict";
    var r = [ "click", "dblclick", "mousedown", "mouseenter", "mouseleave", "mouseup", "mouseover", "mousemove", "mouseout", "keypress", "keydown", "keyup" ];
    t.behaviors.bootstrapDropdown = {
        attach: function(e) {
            var t = e.querySelectorAll(".dropdown [data-dropdown-target]");
            for (var o in t) if (t.hasOwnProperty(o)) for (var a = t[o], n = 0, u = r.length; n < u; n++) {
                var s = r[n];
                a.removeEventListener(s, this.proxyEvent), a.addEventListener(s, this.proxyEvent);
            }
        },
        proxyEvent: function(e) {
            if (!e.type.match(/^key/) || 9 !== e.which && 9 !== e.keyCode) {
                var t = e.currentTarget.dataset && e.currentTarget.dataset.dropdownTarget || e.currentTarget.getAttribute("data-dropdown-target");
                if (t) {
                    e.preventDefault(), e.stopPropagation();
                    var r = t && "#" !== t && document.querySelectorAll(t)[0];
                    r ? o.simulate(r, e.type, e) : o.settings.dev && window.console && !e.type.match(/^mouse/) && window.console.debug("[Drupal Bootstrap] Could not find a the target:", t);
                }
            }
        }
    };
}(jQuery, Drupal, Drupal.bootstrap);