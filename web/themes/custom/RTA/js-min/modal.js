!function(o, t, a) {
    "use strict";
    a.extendPlugin("modal", function(o) {
        return {
            DEFAULTS: {
                animation: !!o.modal_animation,
                backdrop: "static" === o.modal_backdrop ? "static" : !!o.modal_backdrop,
                keyboard: !!o.modal_keyboard,
                show: !!o.modal_show,
                size: o.modal_size
            }
        };
    }), a.replacePlugin("modal", function() {
        var t = this, a = Array.prototype.slice.call(arguments, 1);
        return function(e, i) {
            var n = void 0;
            return this.each(function() {
                var d = o(this), s = d.data("bs.modal"), l = o.extend({}, t.DEFAULTS, d.data(), "object" == typeof e && e);
                s || d.data("bs.modal", s = new t(this, l)), "string" == typeof e ? n = s[e].apply(s, a) : l.show && s.show(i);
            }), 1 === this.length && void 0 !== n ? n : this;
        };
    }), o.extend(t.theme, {
        bootstrapModal: function(a) {
            var e = drupalSettings.bootstrap || {}, i = {
                body: "",
                closeButton: !0,
                description: {
                    content: null,
                    position: "before"
                },
                footer: "",
                id: "drupal-modal",
                size: e.modal_size ? e.modal_size : "",
                title: t.t("Loading...")
            };
            a = o.extend(!0, {}, i, a);
            var n = "", d = [ "modal" ];
            e.modal_animation && d.push("fade"), n += '<div id="' + a.id + '" class="' + d.join(" ") + '" tabindex="-1" role="dialog">';
            var s = [ "modal-dialog" ];
            return a.size && s.push(t.checkPlain(a.size)), n += '<div class="' + s.join(" ") + '" role="document">', 
            n += '<div class="modal-content">', n += t.theme.bootstrapModalHeader(a.title, a.closeButton), 
            n += t.theme.bootstrapModalBody(a.id + "--body", a.body, a.description), n += t.theme.bootstrapModalFooter(a.footer), 
            n += "</div>", n += "</div>", n += "</div>";
        },
        bootstrapModalBody: function(t, a, e) {
            var i = "";
            i += '<div id="' + t + '" class="modal-body">', e && o.isPlainObject(e) || (e = {
                content: e
            }), e = o.extend({
                position: "before"
            }, e);
            var n = [ "help-block" ];
            return e.content && "invisible" === e.position && n.push("sr-only"), e.content && "before" === e.position && (i += '<p class="' + n.join(" ") + '">' + e.content + "</p>"), 
            i += a, !e.content || "after" !== e.position && "invisible" !== e.position || (i += '<p class="' + n.join(" ") + '">' + e.content + "</p>"), 
            i += "</div>";
        },
        bootstrapModalClose: function() {
            return '<button type="button" class="close" data-dismiss="modal" aria-label="' + t.t("Close") + '"><span aria-hidden="true">&times;</span></button>';
        },
        bootstrapModalFooter: function(o, t) {
            return o || t ? '<div class="modal-footer">' + (o || "") + "</div>" : "";
        },
        bootstrapModalHeader: function(o, a) {
            var e = "";
            return o && (a = void 0 === a || a, e += '<div class="modal-header">', a && (e += t.theme.bootstrapModalClose()), 
            e += '<h4 class="modal-title">' + t.checkPlain(o) + "</h4>", e += "</div>"), e;
        }
    });
}(window.jQuery, window.Drupal, window.Drupal.bootstrap);