function rightTime(t) {
    var e = jQuery("#trip_planner_form .time-field"), t = jQuery("#trip_planner_form .date-field"), n = new Date(), a = n.getMinutes(), i = n.getHours(), o = "";
    a = a < 10 ? "0" + n.getMinutes() : n.getMinutes(), i > 12 ? (i -= "12", o = "pm") : 12 == i ? o = "pm" : i < 1 ? (i = "12", 
    o = "am") : o = "am";
    var r = i + ":" + a + o, l = n.getMonth() + 1, c = l < 10 ? "0" + l : "" + l, s = n.getDate(), p = s < 10 ? "0" + s : "" + s, h = c + "/" + p + "/" + n.getFullYear();
    e.val(r), t.val(h);
}

!function(t, e) {
    t(".totop").on("click", function() {
        t("body").scrollTop(0);
    }), e.behaviors.mobilenavBehavior = {
        attach: function(e, n) {
            t(".hamburger").on("click", function() {
                t(this).toggleClass("is-active"), t("nav.header").slideToggle(), t("body , .canvas").toggleClass("menu-open");
            }), t(".main-nav li.expanded").each(function() {
                t(this).on("click", function() {
                    t(this).toggleClass("open");
                });
            });
        }
    };
    var n = window.location.pathname, a = n.split("/");
    t(".menu.main li").each(function() {
        t("a", this).attr("href").split("/")[1] == a[1] && t(this).addClass("active");
    }), 0 == t(".tripblock").length && (t("#trip_planner_buttons").insertAfter(t(".tripplanner h2")), 
    t(".tripplanner").wrapAll('<div class="trip-page" />')), t(".hamburger").on("click", function() {
        t(this).parent().parent().toggleClass("active");
    }), rightTime(), e.behaviors.eventTracking = {
        attach: function(e, n) {
            t("a[href$='.pdf']:not(.noIcon)", e).once("eventTracking").on("click", function() {
                var e = t(this).text();
                t(this).attr("href"), window.location.pathname;
                ga("send", {
                    hitType: "event",
                    eventCategory: "PDF",
                    eventAction: "click",
                    eventLabel: e
                });
            });
        }
    };
}(jQuery, Drupal);