var Drupal = Drupal || {};

!function(o, e, r) {
    "use strict";
    r.extendPlugin("popover", function(o) {
        return {
            DEFAULTS: {
                animation: !!o.popover_animation,
                enabled: o.popover_enabled,
                html: !!o.popover_html,
                placement: o.popover_placement,
                selector: o.popover_selector,
                trigger: o.popover_trigger,
                triggerAutoclose: !!o.popover_trigger_autoclose,
                title: o.popover_title,
                content: o.popover_content,
                delay: parseInt(o.popover_delay, 10),
                container: o.popover_container
            }
        };
    }), e.behaviors.bootstrapPopovers = {
        attach: function(e) {
            if (o.fn.popover && o.fn.popover.Constructor.DEFAULTS.enabled) {
                if (o.fn.popover.Constructor.DEFAULTS.triggerAutoclose) {
                    var r = null;
                    o(document).on("show.bs.popover", "[data-toggle=popover]", function() {
                        var e = o(this);
                        "click" === e.data("bs.popover").options.originalTrigger && (r && !r.is(e) && r.popover("hide"), 
                        r = e);
                    }).on("click", function(e) {
                        var t = o(e.target);
                        t.is("[data-toggle=popover]") && t.data("bs.popover");
                        !r || t.is("[data-toggle=popover]") || t.closest(".popover.in")[0] || (r.popover("hide"), 
                        r = null);
                    });
                }
                for (var t = o(e).find("[data-toggle=popover]").toArray(), p = 0; p < t.length; p++) {
                    var n = o(t[p]), a = o.extend({}, o.fn.popover.Constructor.DEFAULTS, n.data());
                    a.originalTrigger = a.trigger, "click" === a.trigger && (a.trigger = "manual");
                    var i = o(a.target || n.is('a[href^="#"]') && n.attr("href")).clone();
                    !a.content && i[0] && (i.removeClass("visually-hidden hidden").removeAttr("aria-hidden"), 
                    a.content = i.wrap("<div/>").parent()[a.html ? "html" : "text"]() || ""), n.popover(a), 
                    "click" === a.originalTrigger && n.off("click.drupal.bootstrap.popover").on("click.drupal.bootstrap.popover", function(e) {
                        o(this).popover("toggle"), e.preventDefault(), e.stopPropagation();
                    });
                }
            }
        },
        detach: function(e) {
            o.fn.popover && o.fn.popover.Constructor.DEFAULTS.enabled && o(e).find('[data-toggle="popover"]').off("click.drupal.bootstrap.popover").popover("destroy");
        }
    };
}(window.jQuery, window.Drupal, window.Drupal.bootstrap);