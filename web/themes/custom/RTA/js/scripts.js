(function ($, Drupal) {
  //

  //scroll to top of page
  $('.totop').on('click', function(){
    $('body').scrollTop(0);
  });

  Drupal.behaviors.mobilenavBehavior = {
    attach: function (context, settings) {
      //hamburger nav
  //$('#block-rta-main-menu').affix();
      $('.hamburger').on('click', function(){
          $(this).toggleClass('is-active');
          //show links
          $('nav.header').slideToggle();
          //slide body
          $('body , .canvas').toggleClass('menu-open');

          //$('.canvas').offcanvas('hide');
      });

      //dropdown nav
        $('.main-nav li.expanded').each(function(){
            $(this).on('click', function(){
                $(this).toggleClass('open');
            });
        });
    }
  };
  var pathname = window.location.pathname; // Returns path only
  var current = pathname.split('/');
  $('.menu.main li').each(function(){
    var item = $('a', this).attr('href');
    var menu = item.split('/');
    if(menu[1] == current[1]){
        $(this).addClass('active');
    }
  });

  //moving buttons to top of form cause i dont want to make a whole new form
  if($('.tripblock').length == 0 ){
      $('#trip_planner_buttons').insertAfter($('.tripplanner h2'));
    $('.tripplanner').wrapAll('<div class="trip-page" />');
  }
  $('.hamburger').on('click', function(){
    var box = $(this).parent().parent();
    box.toggleClass('active');
  });
  //$('.trip-page #trip_planner_buttons').insertBefore($('.trip-page #trip_planner_form'));
  rightTime();




  Drupal.behaviors.eventTracking = {
    attach: function (context, settings) {
      $("a[href$='.pdf']:not(.noIcon)", context).once('eventTracking').on('click', function(){
        var name = $(this).text();
        var url = $(this).attr('href');
        var current = window.location.pathname;
        //console.log(name, url, current);
        ga('send', {
            hitType: 'event',
            eventCategory: 'PDF',
            eventAction: 'click',
            eventLabel: name
        });
      });
    }
  }
})(jQuery, Drupal);

function rightTime(date){
  var time = jQuery('#trip_planner_form .time-field');
  var date = jQuery('#trip_planner_form .date-field');
  var today = new Date(),
  min = today.getMinutes(),
  hours = today.getHours(),
  ampm = '';

  if(min < 10){
    min = '0' + today.getMinutes();
  } else{
    min = today.getMinutes();
  }

  if(hours > 12){
    hours = hours - '12';
    ampm = 'pm';
  } else if(hours == 12){
    ampm = 'pm';
  }else if(hours < 1){
    hours = '12';
    ampm = 'am';
  }else{
    ampm = 'am';
  }
  var currentTime = hours + ":" + min + ampm;
  var month = today.getMonth() + 1;
  var cMonth = month < 10 ? '0' + month : '' + month;

  var day = today.getDate();
  var cDay = day < 10 ? '0' + day : '' + day;

  var currentDate = cMonth + '/' + cDay + '/' + today.getFullYear();
  time.val(currentTime);
  date.val(currentDate);
  //time.attr('placeholder', 'currentTime');
}