/*
Custom functionality for safari and IE
 */
(function( d ) {
	// In case the placeholder functionality is available we remove labels
	var sub = document.getElementById('subscribe-submit');
	if(sub){
		if ( ( 'placeholder' in d.createElement( 'input' ) ) ) {
			var label = d.querySelector( 'label[for=subscribe-field-blog_subscription-7]' );
				label.style.clip 	 = 'rect(1px, 1px, 1px, 1px)';
				label.style.position = 'absolute';
				label.style.height   = '1px';
				label.style.width    = '1px';
				label.style.overflow = 'hidden';
		}

		// Make sure the email value is filled in before allowing submit
		var form = d.getElementById('subscribe-blog-blog_subscription-7'),
			input = d.getElementById('subscribe-field-blog_subscription-7'),
			handler = function( event ) {
				if ( '' === input.value ) {
					input.focus();

					if ( event.preventDefault ){
						event.preventDefault();
					}

					return false;
				}
			};

	if(form){
		if ( window.addEventListener ) {
			form.addEventListener( 'submit', handler, false );
		} else {
			form.attachEvent( 'onsubmit', handler );
		}
	}
}
})( document );
