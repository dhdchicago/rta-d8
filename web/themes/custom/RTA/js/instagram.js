(function ($, Drupal) {

const url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=1758623324.f4ad936.b4f021fede03434fa77dbc7727fb7ac6';

$.ajax({
  url: url,
  type: 'GET',
  data: {},
  dataType: 'json',
  success: function (response) {

//window.fetch(url)
  //.then(function(response) {
	//return response.json();
  //})
  //.then(function(feed) {
    response.data.forEach(function (item) {
      var image = item.images.standard_resolution.url,
      caption = item.caption.text,
      captionDiv = document.createElement('div'),
      imageTag = document.createElement('img'),
      slides = document.createElement('div'),
      href = document.createElement('a'),
      //url = item.link;
      url = 'https://www.instagram.com/rta_chicago/';

      if(caption.length > 120 ){
        caption = caption.substring(0, 120)+"...";
      }

      imageTag.src = image;
      href.setAttribute('href', url);
      href.setAttribute('target', '_blank');
      href.appendChild(imageTag);
      captionDiv.innerHTML = caption;
      captionDiv.classList.add('text');
      slides.appendChild(href);
      slides.appendChild(captionDiv);
      slides.classList.add('mySlides', 'fade');
      var container = document.getElementById('instaSlide');
      if (container){
        container.appendChild(slides);
      }
    });

    var prev = document.createElement('div'),
    next = document.createElement('div');
      prev.setAttribute('id', 'prev');
      next.setAttribute('id', 'next');
      next.innerHTML = '❯';
      prev.innerHTML = '❮';

    var container = document.getElementById('instaSlide');
    if(container){
      var n = container.childElementCount;
      container.appendChild(next);
      container.appendChild(prev);
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    prev.addEventListener('click', function(){
      if(slideIndex > 1 ){
        showSlides(slideIndex += -1);
      }
    }, false);

    next.addEventListener('click', function(){
      if(slideIndex <= n){
        showSlides(slideIndex += 1);
      }
    }, false);
  }
});


function showSlides(slideIndex) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  //var dots = document.getElementsByClassName("dot");
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }
  if (slideIndex < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  if(slides.length > 0){
    slides[slideIndex-1].style.display = "block";
  }
}


})(jQuery, Drupal);
